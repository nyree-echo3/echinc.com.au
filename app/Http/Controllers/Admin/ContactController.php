<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Setting;
use App\ContactMessages;
use Illuminate\Support\Facades\Response;


class ContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        $session = session()->get('message-filter');

        if ($is_filtered) {
            $messages = ContactMessages::Filter()->orderBy('created_at', 'desc')->paginate($paginate_count);
        } else {
            $messages = ContactMessages::orderBy('created_at', 'desc')->paginate($paginate_count);
        }

        return view('admin/contact/inbox', array(
            'messages' => $messages,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function formBuilder()
    {
        $form = Setting::where('key', '=', 'contact-form-fields')->first();

        return view('admin/contact/form-builder', array(
            'form' => $form->value
        ));
    }

    public function saveForm(Request $request)
    {
        $name_field_control = false;
        $email_field_control = false;
        $message_field_control = false;

        $fields = json_decode($request->form);
        foreach ($fields as $field) {
            if ($field->type == 'text' && $field->name == 'name') {
                $name_field_control = true;
            }

            if ($field->type == 'text' && $field->subtype == 'email' && $field->name == 'email') {
                $email_field_control = true;
            }

            if ($field->type == 'textarea' && $field->name == 'message') {
                $message_field_control = true;
            }
        }

        if(!$name_field_control){
            return Response::json([
                'status' => 'fail',
                'message' => 'Please create a Text Field with the name parameter is "name"'
            ]);
        }

        if(!$email_field_control){
            return Response::json([
                'status' => 'fail',
                'message' => 'Please create a Text Field with the name parameter is "email" and the type is "email"'
            ]);
        }

        if(!$message_field_control){
            return Response::json([
                'status' => 'fail',
                'message' => 'Please create a Text Area with the name parameter is "message"'
            ]);
        }

        $form = Setting::where('key', '=', 'contact-form-fields')->first();
        $form->value = $request->form;
        $form->save();

        return Response::json(['status' => 'success']);
    }

    public function details()
    {
        $details = Setting::where('key', '=', 'contact-details')->first();
        return view('admin/contact/details', array(
            'details' => $details
        ));
    }

    public function store(Request $request)
    {

        $details = Setting::where('key', '=', 'contact-details')->first();
        $details->value = $request->details;
        $details->save();

        return \Redirect::to('dreamcms/contact/details')->with('message', Array('text' => 'Details has been updated', 'status' => 'success'));
    }

    public function delete(Request $request)
    {

        if ($request->messages) {

            foreach ($request->messages as $message) {
                $msg = ContactMessages::where('id','=',$message)->first();
                $msg->is_deleted = true;
                $msg->save();
            }

            return \Redirect::to('dreamcms/contact')->with('message', Array('text' => 'Messages have been deleted', 'status' => 'success'));

        } else {

            return \Redirect::to('dreamcms/contact')->with('message', Array('text' => 'Please select messages', 'status' => 'error'));
        }

    }

    public function deleteSingle($message_id)
    {
        $msg = ContactMessages::where('id','=',$message_id)->first();
        $msg->is_deleted = true;
        $msg->save();

        return \Redirect::to('dreamcms/contact')->with('message', Array('text' => 'Message has been deleted', 'status' => 'success'));
    }

    public function read($message_id)
    {
        $previous = ContactMessages::where('id', '<', $message_id)->max('id');
        $next = ContactMessages::where('id', '>', $message_id)->min('id');

        $message = ContactMessages::where('id', '=', $message_id)->first();
        $message->status = 'read';
        $message->save();

        return view('admin/contact/read', array(
            'message' => $message,
            'previous' => $previous,
            'next' => $next
        ));
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->search) {
            $filter_control = true;
        } else {
            session()->forget('message-filter');
        }

        if ($filter_control) {
            $request->session()->put('message-filter', [
                'search' => $request->search
            ]);
        }

        if (session()->has('message-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

}

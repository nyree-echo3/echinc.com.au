<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Images;
use App\GalleryCategory;
use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $is_filtered = $this->isFiltered($request);
        $paginate_count = session()->get('pagination-count');

        if ($is_filtered) {
            $images = Images::Filter()->orderBy('position', 'desc')->paginate($paginate_count);
        } else {
            $images = Images::with('category')->orderBy('position', 'desc')->paginate($paginate_count);
        }

        $session = session()->get('gallery-filter');
        $categories = GalleryCategory::orderBy('created_at', 'desc')->get();
        return view('admin/gallery/gallery', array(
            'images' => $images,
            'categories' => $categories,
            'is_filtered' => $is_filtered,
            'session' => $session
        ));
    }

    public function add()
    {
        $categories = GalleryCategory::orderBy('created_at', 'desc')->get();
        return view('admin/gallery/add', array(
            'categories' => $categories
        ));
    }

    public function edit($image_id)
    {
        $image = Images::where('id', '=', $image_id)->first();
        $categories = GalleryCategory::orderBy('created_at', 'desc')->get();
        return view('admin/gallery/edit', array(
            'image' => $image,
            'categories' => $categories
        ));
    }

    public function store(Request $request)
    {
        $rules = array(
            'category_id' => 'required',
            'name' => 'required',
			'name_cn' => 'required',
            'location' => 'required',
        );

        $messages = [
            'category_id.required' => 'Please select category',
            'name.required' => 'Please enter name (English)',           
			'name_cn.required' => 'Please enter name (Chinese)',           
            'location.required' => 'Please select an image',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/gallery/add')->withErrors($validator)->withInput();
        }

        $image = new Images();
        $image->category_id = $request->category_id;
        $image->name = $request->name;
		$image->name_cn = $request->name_cn;
        $image->location = $request->location;
        $image->description = $request->description;

        if($request->live=='on'){
           $image->status = 'active'; 
        }

        $image->position = Images::max('position')+1;
        $image->save();
      
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/gallery/' . $image->id . '/edit')->with('message', Array('text' => 'Image has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/gallery/')->with('message', Array('text' => 'Image has been added', 'status' => 'success'));
		}		        


    }

    public function update(Request $request)
    {
        $rules = array(
            'category_id' => 'required',
            'name' => 'required',
			'name_cn' => 'required',
            'location' => 'required'
        );

        $messages = [
            'category_id.required' => 'Please select category',
            'name.required' => 'Please enter name (English)',
			'name_cn.required' => 'Please enter name (Chinese)',
            'location.required' => 'Please select an image'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/gallery/' . $request->id . '/edit')->withErrors($validator)->withInput();
        }

        $image = Images::where('id','=',$request->id)->first();
        $image->category_id = $request->category_id;
        $image->name = $request->name;
		$image->name_cn = $request->name_cn;
		$image->location = $request->location;
        $image->description = $request->description;
		if($request->live=='on'){
           $image->status = 'active'; 
		} else {
			$image->status = 'passive';
        }
        $image->save();
       
		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/gallery/' . $image->id . '/edit')->with('message', Array('text' => 'Image has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/gallery/')->with('message', Array('text' => 'Image has been updated', 'status' => 'success'));
		}
    }

    public function delete($image_id)
    {
        $news = Images::find($image_id);
        $news->delete();

        return \Redirect::back()->with('message', Array('text' => 'Image has been deleted.', 'status' => 'success'));
    }

    public function changeStatus(Request $request, $image_id)
    {
        $image = Images::where('id', '=', $image_id)->first();
        if ($request->status == "true") {
            $image->status = 'active';
        } else if ($request->status == "false") {
            $image->status = 'passive';
        }
        $image->save();

        return Response::json(['status' => 'success']);
    }

    public function categories()
    {
        $categories = GalleryCategory::orderBy('position', 'desc')->get();
        return view('admin/gallery/categories', compact('categories'));
    }

    public function addCategory()
    {
        return view('admin/gallery/add-category');
    }

    public function storeCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_store:gallery_categories'
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_store' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/gallery/add-category')->withErrors($validator)->withInput();
        }

        $category = new GalleryCategory();
        $category->name = $request->name;
		$category->name_cn = $request->name_cn;
        $category->slug = $request->slug;
		$category->description = $request->description;
        if($request->live=='on'){
            $category->status = 'active';
        }
        $category->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/gallery/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/gallery/categories')->with('message', Array('text' => 'Category has been added', 'status' => 'success'));
		}

    }

    public function editCategory($category_id)
    {
        $category = GalleryCategory::where('id', '=', $category_id)->first();
        return view('admin/gallery/edit-category', array(
            'category' => $category,
        ));
    }

    public function updateCategory(Request $request)
    {
        $rules = array(
            'name' => 'required',
            'slug' => 'required|unique_update:gallery_categories,'.$request->id
        );

        $messages = [
            'name.required' => 'Please enter category name.',
            'slug.unique_update' => 'Seo name is in use'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('dreamcms/gallery/' . $request->id . '/edit-category')->withErrors($validator)->withInput();
        }

        $category = GalleryCategory::findOrFail($request->id);
        $category->name = $request->name;
		$category->name_cn = $request->name_cn;
        $category->slug = $request->slug;
		$category->description = $request->description;
        if($request->live=='on'){
           $category->status = 'active'; 
		} else {
			$category->status = 'passive';
        }
        $category->save();

		if ($request->get('action') == 'save') {			
		   return \Redirect::to('dreamcms/gallery/' . $category->id . '/edit-category')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		} else {
           return \Redirect::to('dreamcms/gallery/categories')->with('message', Array('text' => 'Category has been updated', 'status' => 'success'));
		}
    }

    public function deleteCategory($category_id)
    {
        $category = GalleryCategory::where('id','=',$category_id)->first();

        if(count($category->images)){
            return \Redirect::to('dreamcms/gallery/categories')->with('message', Array('text' => 'Category has images. Please delete images first.', 'status' => 'error'));
        }

        $category->is_deleted = true;
        $category->save();

        return \Redirect::to('dreamcms/gallery/categories')->with('message', Array('text' => 'Category has been deleted.', 'status' => 'success'));
    }

    public function changeCategoryStatus(Request $request, $category_id)
    {
        $category = GalleryCategory::where('id', '=', $category_id)->first();
        if ($request->status == "true") {
            $category->status = 'active';
        } else if ($request->status == "false") {
            $category->status = 'passive';
        }
        $category->save();

        return Response::json(['status' => 'success']);
    }

    public function sortCategory()
    {
        $categories = GalleryCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        return view('admin/gallery/sort-category', array(
            'categories' => $categories
        ));
    }

    public function imageSort(Request $request)
    {
        $image_count = count($request->item);

        foreach($request->item as $image_id){
            $image = Images::where('id','=',$image_id)->first();
            $image->position = $image_count;
            $image->save();
            $image_count--;
        }
        return Response::json(['status' => 'success']);
    }

    public function emptyFilter()
    {
        session()->forget('gallery-filter');
        return redirect()->to('dreamcms/gallery');
    }

    public function isFiltered($request)
    {

        $filter_control = false;

        if ($request->category && $request->category != "all") {
            $filter_control = true;
        }

        if ($request->search) {
            $filter_control = true;
        }

        if ($filter_control) {
            $request->session()->put('gallery-filter', [
                'category' => $request->category,
                'search' => $request->search
            ]);
        }

        if (session()->has('gallery-filter')) {
            $filter_control = true;
        }

        return $filter_control;
    }

}
<?php

namespace App\Http\Controllers\Site;

use App\ContactMessages;
use App\Http\Controllers\Controller;
use App\Mail\ContactMessageAdmin;
use App\Mail\ContactMessageUser;
use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator, Illuminate\Support\Facades\Input, Redirect;

use App\Module;

class ContactController extends Controller
{
    public function index(){
		$module = Module::where('slug', '=', "contact")->first();

        $fields = Setting::where('key', '=', 'contact-form-fields')->first();

        // Contact Details
        $contact_details = Setting::where('key', '=', 'contact-details')->first();
		
		// Address Details
        $address = Setting::where('key', '=', 'address')->first();
		
		// Email Details
        $email = Setting::where('key', '=', 'email')->first();
		$email2 = Setting::where('key', '=', 'email2')->first();
		
		// Phone Details
        $phone_number = Setting::where('key', '=', 'phone-number')->first();
		$phone_number2 = Setting::where('key', '=', 'phone-number2')->first();
		
		// Fax Details
        $fax_number = Setting::where('key', '=', 'fax-number')->first();		
		
		// Google Map Details
        $google_map = Setting::where('key', '=', 'google-map')->first();		

        $form = $fields->value;

        if(old('name')){
            $fields = json_decode($fields->value);
            foreach ($fields as $field){
                $field->value = old($field->name);
            }

            $form = json_encode($fields);
        }

        return view('site/contact/contact', array(
            'form' => $form,
            'contact_details' => $contact_details->value,
			'module' => $module,
			'address' => $address->value,
			'email' => $email->value,
			'email2' => $email2->value,
			'phone_number' => $phone_number->value,
			'phone_number2' => $phone_number2->value,
			'fax_number' => $fax_number->value,
			'google_map' => $google_map->value
        ));
    }

    public function saveMessage(Request $request)
    {

        $rules = array(
            'g-recaptcha-response' => 'required|recaptcha'
        );

        $messages = [
            'g-recaptcha-response.required' => 'Please verify yourself',
            'g-recaptcha-response.recaptcha' => 'Please verify yourself'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect('contact')->withErrors($validator)->withInput();
        }

        $fields = Setting::where('key', '=', 'contact-form-fields')->first();
        $fields = json_decode($fields->value);

        $data = array();

        foreach ($fields as $field){

            if($field->type!='header' && $field->type!='paragraph'){

                if($field->type=='date'){
                    $date_array = explode('-',$request->{$field->name});
                     $request->{$field->name} = $date_array[2].'-'.$date_array[1].'-'.$date_array[0];
                }

                $data_field['label'] = $field->label;
                $data_field['field'] = $field->name;
                $data_field['value'] = $request->{$field->name};
                array_push($data, $data_field);
            }
        }

        $message = new ContactMessages();
        $message->data = json_encode($data);
        $message->save();

		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
		// Email Website Owner
        Mail::to($contactEmail)->send(new ContactMessageAdmin($message));
		
		// Email User
        Mail::to($request->email)->send(new ContactMessageUser($message));

        return \Redirect::to('contact/success');
    }

    public function success(){

        return view('site/contact/success');
    }
}

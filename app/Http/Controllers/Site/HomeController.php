<?php

namespace App\Http\Controllers\Site;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

    	return view('site/index');

    }
	
	public function styleGuide(){

    	return view('site/style-guide');

    }
	
	public function language($language = ""){    	   		
		Session::put('language', $language);		
    }
}

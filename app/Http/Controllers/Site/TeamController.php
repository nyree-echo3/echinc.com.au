<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TeamMember;
use App\TeamCategory;
use App\Module;

class TeamController extends Controller
{
    public function index(Request $request)
    {
        $module = Module::where('slug', '=', "team")->first();
		
        $categories = TeamCategory::where('status','=','active')->orderBy('position', 'desc')->get();

        if($request->category==''){
            $selected_category = TeamCategory::where('status','=','active')->whereHas('members')->orderBy('position', 'desc')->first();
        }else{
            $selected_category = TeamCategory::where('slug','=',$request->category)->first();
        }

        $items = null;
        $meta_title_inner = "Team";
        $meta_keywords_inner = "Team";
        $meta_description_inner = "Team";
		
		$all_team_members = TeamMember::where('status','=','active')->with("category")->orderBy('position','asc')->get();
        $first_team_member = $all_team_members->first();

        if($selected_category){
            $items = TeamMember::where('category_id','=', $selected_category->id)->paginate(10);

            $meta_title_inner = $selected_category->name. " - Team";
            $meta_description_inner = $selected_category->name . " - Team";
        }

        return view('site/team/list', array(
			'all_team_members' => $all_team_members,
			'team_members' => $all_team_members,
			'module' => $module,
            'categories' => $categories,
            'category' => $selected_category,
            'items' => $items,
            'meta_title_inner' => $meta_title_inner,
            'meta_keywords_inner' => $meta_keywords_inner,
            'meta_description_inner' => $meta_description_inner
        ));

    }

    public function member($category, $team_member)
    {
		$module = Module::where('slug', '=', "team")->first();
		
		$all_team_members = TeamMember::where('status','=','active')->with("category")->get();
        $team_member = TeamMember::where('slug','=',$team_member)->first();

        $meta_title_inner = "Team - ".$team_member->name;
        $meta_keywords_inner = "Team".$team_member->name;
        $meta_description_inner = "Team".$team_member->name;

        return view('site/team/item', array(
			'module' => $module,
            'all_team_members' => $all_team_members,
            'team_member' => $team_member,
            'header_image' => $this->headerImage(),
            'meta_title_inner' => $meta_title_inner,
            'meta_keywords_inner' => $meta_keywords_inner,
            'meta_description_inner' => $meta_description_inner
        ));
	}
	
	private function headerImage()
    {
        $module = Module::where('slug','=','team')->first();

        if($module->header_image){

            $header_image =  $module->header_image;
        }else{

            $home_slider = ImagesHomeSlider::where('status', '=', 'active')->orderBy('position', 'desc')->first();
            $header_image = $home_slider->location;
        }

        return $header_image;
    }
}

<?php

namespace App\Mail;

use App\ContactMessages;
use App\Setting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactMessageUser extends Mailable
{
    use Queueable, SerializesModels;

    public $contact_message;

    public function __construct(ContactMessages $contact_message)
    {
        $this->contact_message = $contact_message;
    }

    public function build()
    {
		$setting = Setting::where('key','=','company-name')->first();
		$companyName = $setting->value;
		
		$setting = Setting::where('key','=','contact-details')->first();
		$contactDetails = $setting->value;
		
		$setting = Setting::where('key','=','contact-email')->first();
		$contactEmail = $setting->value;
		
        return $this->subject($companyName)
			        ->from($contactEmail)
			        ->view('site/emails/contact-message-user', array(
						'companyName' => $companyName, 
						'contactDetails' => $contactDetails, 
					));
    }
}

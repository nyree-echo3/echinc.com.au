<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class NewsCategory extends Model
{
    use SortableTrait;

    protected $table = 'news_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function news()
    {
        return $this->hasMany(News::class, 'category_id')->where('status', '=', 'active');
    }
	
	public function active_news()
    {
        return $this->hasMany(News::class, 'category_id')->where('status', '=', 'active');
    }
}

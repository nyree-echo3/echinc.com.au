<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class ProductCategory extends Model
{
    use SortableTrait;

    protected $table = 'product_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function products()
    {
        return $this->hasMany(Products::class, 'category_id');
    }
}

@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Member Types</h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('dreamcms/members/types') }}"><i class="fa fa-users"></i> Member Types</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Member Type List</h3>
                    <div class="pull-right box-tools">
                        @can('sort-members-type')
                            <a href="{{ url('dreamcms/members/sort-type') }}" type="button" class="btn bg-olive btn-sm"
                               data-widget="add">Sort
                                <i class="fa fa-list-ul"></i>
                            </a>
                        @endcan
                        @can('add-members-type')
                            <a href="{{ url('dreamcms/members/add-type') }}" type="button" class="btn btn-info btn-sm"
                               data-widget="add">Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        @endcan
                    </div>
                </div>
                <div class="box-body">
                    @if(count($types))
                        <table class="table table-hover">
                            <tr>
                                <th>Name</th>
                                <th>Price</th>
                                <th>@sortablelink('status')</th>
                                <th class="hd-text-right">Actions</th>
                            </tr>

                            @foreach($types as $type)
                                <tr>
                                    <td>{{ $type->name }}</td>
                                    <td>${{ number_format($type->price,2) }}</td>
                                    <td>
                                        <input id="member_{{ $type->id }}" data-id="{{ $type->id }}"
                                               class="member_status" type="checkbox" data-toggle="toggle"
                                               data-size="mini"{{ $type->status == 'active' ? ' checked' : null }}>
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            @can('edit-members-type')
                                                <a href="{{ url('dreamcms/members/'.$type->id.'/edit-type') }}"
                                                   class="tool" data-toggle="tooltip" title="Edit">
                                                    <i class="fa fa-edit"></i></a>
                                            @endcan
                                            @can('delete-members-type')
                                                <a href="{{ url('dreamcms/members/'.$type->id.'/delete-type') }}"
                                                   class="tool" data-toggle=confirmation data-title="Are you sure?"
                                                   data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                                   data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                            @endcan
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>

            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.member_status').change(function () {
                $.ajax({
                    type: "POST",
                    url: $(this).data('id') + "/change-type-status",
                    data: {
                        'status': $(this).prop('checked')
                    },
                    success: function (response) {
                        if (response.status == "success") {
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    }
                });
            });

        });
    </script>
@endsection
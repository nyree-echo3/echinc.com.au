@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/src/baguetteBox.css') }}">
@endsection

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-gallery')        
        
        <div class="col-sm-8 blog-main">
          <div class="blog-post">
          
			  <h1 class="blog-post-title">{{ (Session::get('language') != "CN" ? $category->name : $category->name_cn) }}</h1>

			  @if($category->description != "")
				 {!! $category->description !!}
			  @endif

			  @if(isset($items)) 
				 <section class="gallery-block cards-gallery">
					<div class="container">	        
					   <div class="row">

							 @foreach ($items as $item)	

								 <div class="col-md-6 col-lg-4">
								    <a class="lightbox" href="{{ url('') }}{{$item->location}}" data-caption="{{ (Session::get('language') != "CN" ? $item->name : $item->name_cn) }}<!--<br>{!! $item->description !!}-->">
										<div class="card border-0 transform-on-hover">
											<img src="{{ url('') }}{{$item->location}}" alt="{{$item->name}}" class="card-img-top">

											<div class="card-body">
												<h6>{{ (Session::get('language') != "CN" ? $item->name : $item->name_cn) }}</h6>
												<!--<p class="text-muted card-text">{!! $item->description !!}</p>-->
											</div>
										</div>
									</a>
								</div>							   

							 @endforeach

					   </div>
					</div>
				 </section>  
			  @endif			                                                                                                                                                                                                                                                                                                                                                                  
          </div><!-- /.blog-post -->                                                                                                                                                                                        
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection

@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/src/baguetteBox.js') }}"></script>
@endsection

@section('inline-scripts')
   <script type="text/javascript">
        $(document).ready(function () {       
           baguetteBox.run('.cards-gallery', { animation: 'slideIn'});
        });
    </script>			
@endsection
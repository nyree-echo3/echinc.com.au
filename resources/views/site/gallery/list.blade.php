@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-gallery')        
        
        <div class="col-sm-8 blog-main">
          <div class="blog-post">
			  <h1 class="blog-post-title">{{ (Session::get('language') != "CN" ? $module->display_name : $module->display_name_cn) }}</h1>

			  @if(isset($side_nav)) 
				 <section class="gallery-block cards-gallery">
					<div class="container">	        
					   <div class="row">

							 @foreach ($side_nav as $item)	                           
								 <div class="col-md-6 col-lg-4">
								    <a class="lightbox" href="{{ url('') }}/gallery/{{$item->slug}}">
										<div class="card border-0 transform-on-hover">										
											<img src="{{ url('') }}{{$item->images[0]->location}}" alt="{{$item->images[0]->name}}" class="card-img-top">

											<div class="card-body">
												<h6>{{ (Session::get('language') != "CN" ? $item->name : $item->name_cn) }}</h6>
												<p class="text-muted card-text">{!! $item->description !!}</p>
											</div>
										</div>
									</a>
								</div>							   

							 @endforeach

					   </div>
					</div>
				 </section>  
			  @endif			                                                                                                                                                                                                                                                                                                                                                                  
            </div><!-- /.blog-post -->                                                                                                                                                                                         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
@endsection

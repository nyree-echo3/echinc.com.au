@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')

<!-- Intro Text -->
<div class="body">
	<div class="panelNav">		
		<div class="container-fluid home-intro">		
				<div class="row">
					<div class="col-lg-6 home-intro-box">
					    <div class="home-intro-box-hdr">
					    {{ (Session::get('language') != "CN" ? "About" : "关于") }}
						</div>
						
						<img  src="{{ url('') }}/images/site/pic1.jpg">
					</div>

					<div class="col-lg-6 home-intro-box">
					    <div class="home-intro-box-txt">
						   <?php echo $home_intro_text; ?>
							<a href=""><i class="fas fa-angle-down"></i></a>
						</div>
					</div>
				</div>
			</div>
	
	</div>
	
	<div class="body-spacer">
		
	</div>
</div>

@endsection

<footer class='footer'>			
	<div class="panelNav">
		<div class="container-fluid">
			<div class="row">

			   <div class="col-lg-3 col-full">
				  <div class="center-block">
					 <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo-bottom.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
				  </div>
			   </div>  

			   <div class="col-lg-9 col-full">
				  <div class="container-fluid">
					 <div class="row">
						 <div class="col-lg-3 footer-col-contacts">
							 <h2>{{ (Session::get('language') != "CN" ? "Address" : "地址") }}</h2>
							 {!! $address !!}
						 </div>

						 <div class="col-lg-3 footer-col-contacts">
							<h2>{{ (Session::get('language') != "CN" ? "Phone" : "电话") }}</h2>
							 <a href="tel:{{ str_replace(" ", "", $phone_number) }}">{{ $phone_number }}</a><br>
							 <a href="tel:{{ str_replace(" ", "", $phone_number2) }}">{{ $phone_number2 }}</a>
						 </div>

						 <div class="col-lg-3 footer-col-contacts">
							<h2>{{ (Session::get('language') != "CN" ? "Email" : "电子邮件") }}</h2>
							 <a href="mailto:{{ $email }}">{{ $email }}</a><br>
							 <a href="mailto:{{ $email2 }}">{{ $email2 }}</a>
						 </div>

						 <div class="col-lg-3 footer-col-contacts">
							<h2>{{ (Session::get('language') != "CN" ? "Fax" : "传真") }}</h2>
							{{ $fax_number }}
						 </div>
					 </div>

					 <div class="row">
						 <div class="col-lg-12 footer-col-links">
							 <div id="footer-txt"> 
								@if ( $company_name != "")<a href="{{ url('') }}" class="footer-a-first">&copy; {{ date('Y') }} {{ (Session::get('language') != "CN" ? $company_name : "安老之家") }} </a> | @endif 
								<a href="https://www.echo3.com.au" target="_blank">{{ (Session::get('language') != "CN" ? "Website by" : "网站由") }} Echo3</a> |
								<a href="{{ url('') }}/pages/privacy-statement">{{ (Session::get('language') != "CN" ? "Privacy Policy" : "隐私政策") }}</a> | 
								<a href="{{ url('') }}/contact">{{ (Session::get('language') != "CN" ? "Feedback" : "反馈") }}</a>    
								

							  </div>
						 </div>
					  </div>
				  </div>	   	 

			   </div>  		   		   				   		   		   		   

			</div>
		</div> 
	</div>

	
</footer>
<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">  	
       <h5>{{ (Session::get('language') != "CN" ? "Address" : "地址") }}</h5>
	   {!! $address !!}
	   
	   <div class="contact-separator"></div>
	   
	   <h5>{{ (Session::get('language') != "CN" ? "Email" : "电子邮件") }}</h5>
	   <a href='mailto:{{ $email }}'>{{ $email }}</a><br>
	   <a href='mailto:{{ $email2 }}'>{{ $email2 }}</a>
	   
	   <div class="contact-separator"></div>
	   
	   <h5>{{ (Session::get('language') != "CN" ? "Phone" : "电话") }}</h5>
	   <a href='tel:{{ str_replace(" ", "", $phone_number) }}'>{{ $phone_number }}</a><br>
	   <a href='tel:{{ str_replace(" ", "", $phone_number2) }}'>{{ $phone_number2 }}</a>
	   
	   <div class="contact-separator"></div>
	   
	   <h5>{{ (Session::get('language') != "CN" ? "Fax" : "传真") }}</h5>
	   {{ $fax_number }}
	   
	   <div class="contact-separator"></div>
	   
	   <div class="divMap">{!! $google_map !!}</div>
  </div>  
</div>
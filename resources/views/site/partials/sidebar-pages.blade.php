<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>{{ (Session::get('language') != "CN" ? $category[0]->name : $category[0]->name_cn) }}</h4>
	<ol class="navsidebar list-unstyled">             
	  @foreach ($side_nav as $item)
		 <li class='{{ ($item->slug == $page->slug ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/pages/{{ $category[0]->slug }}/{{ $item->slug }}">{{ $item->title }}</a></li>

		 <ol class="navsidebar navsidebar-sub list-unstyled">  
			@foreach ($item->nav_sub as $item_sub)            
			 <li class='{{ ($item_sub->slug == $page->slug ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/pages/{{ $category[0]->slug }}/{{ $item_sub->slug }}">{{ $item_sub->title }}</a></li>             
			@endforeach
		 </ol>
	  @endforeach              
	</ol>
  </div>          
</div>
<div class="col-sm-3 offset-sm-1 blog-sidebar">         
  <div class="sidebar-module">
	<h4>{{ (Session::get('language') != "CN" ? $module->display_name : $module->display_name_cn) }}</h4>
	<ol class="navsidebar list-unstyled">             
	  @php
            $count = 1;
            @endphp
            @foreach ($all_team_members as $member)            
            <li {!! ((Request::segment(3) == $member->slug) || (Request::segment(2)=='' && $count==1)) ? ' class="active"' : null !!}>
                <a class="navsidebar" href="{{ url('') }}/team/{{ $member->category->slug }}/{{ $member->slug }}">{{ (Session::get('language') != "CN" ? $member->name : $member->name_cn) }}</a>
            </li>
            @php
            $count++;
            @endphp
            @endforeach       
	</ol>
  </div>          
</div>
@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/src/baguetteBox.css') }}">
@endsection


<div class="blog-masthead ">
    <div class="container">

        <div class="row">
            @include('site/partials/sidebar-team')

            <div class="col-sm-8 blog-main">
                <div class="blog-post">                                      
                    @if ($team_member->photo)
                        <!--<a class="lightbox" href="{{ url('') }}{{$team_member->photo}}"
                           data-caption="{{$team_member->name}}">-->
                            <img src="{{ url('') }}{{$team_member->photo}}" alt="{{$team_member->name}}"
                                 class="card-img-top rounded team-img" align="right" hspace="10">
                        <!--</a>-->
                    @endif
                    
                    <h1>{{ (Session::get('language') != "CN" ? $team_member->name : $team_member->name_cn) }}</h1>
                    
                    <!--
                    @if ($team_member->job_title != "")
                       <h3>{{ $team_member->job_title }}</h3>
                    @endif
                    -->
                    
                    <h2>{{ (Session::get('language') != "CN" ? $team_member->role : $team_member->role_cn) }}</h2>
                                        
                    <!-- <div class="team-qualification">{!! $team_member->short_description !!}</div> -->                   

                    @if($team_member->phone)
                        <p><strong>{{ $team_member->phone }}</strong></p>
                    @endif

                    @if($team_member->mobile)
                        <p><strong>{{ $team_member->mobile }}</strong></p>
                    @endif

                    @if($team_member->email)
                        <p><strong>{{ $team_member->email }}</strong></p>
                    @endif
                     
                    @if(Session::get('language') != "CN")                                     
                       {!! $team_member->body !!}
                    @else
                       {!! $team_member->body_cn !!}
                    @endif
                    
                </div>

            </div><!-- /.blog-post -->
            </div><!-- /.blog-post -->
        </div><!-- /.blog-main -->

    </div><!-- /.row -->

</div><!-- /.container -->

@endsection


@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/src/baguetteBox.js') }}"></script>
@endsection


@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            //baguetteBox.run('.cards-team', {animation: 'slideIn'});
        });
    </script>
@endsection
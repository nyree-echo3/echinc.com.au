@extends('site/layouts/app') 

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        @include('site/partials/sidebar-team')
        
        <div class="col-sm-8 blog-main">

          <div class="blog-post">           
            <h1 class="blog-post-title">{{ (Session::get('language') != "CN" ? $module->display_name : $module->display_name_cn) }}</h1>
	              
            @if(count($team_members))
                  <section class="gallery-block cards-gallery">
                     <div class="container">	  
                       <div class="row">                                     
					     @foreach($team_members as $team_member)
							<div class="col-md-6 col-lg-4">
								<a href="{{ url('/team/'.$team_member->category->slug.'/'.$team_member->slug) }}">
									<div class="card border-0 transform-on-hover">								   
										<img src="{{ url('').$team_member->photo }}" alt="{{ $team_member->name }}" class="card-img-top">

										<div class="team-name">{{ (Session::get('language') != "CN" ? $team_member->name : $team_member->name_cn) }}</div>
										<!--<div class="col-12 team-job-title"><strong>{{ $team_member->job_title }}</strong></div>-->
										<div class="team-job-title">{{ (Session::get('language') != "CN" ? $team_member->role : $team_member->role_cn) }}</div>									
									</div>
								</a>
							</div>
						@endforeach                               
                       </div>
                     </div>
                   </section>  
                   
              
               @else
                 <p>Currently there is no team member to display.</p>
               @endif

          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div>
@endsection